<?php
/**
 * Created by PhpStorm.
 * User: nikolai
 * Date: 09.02.16
 * Time: 10:53
 */
?>
    <div class="avt">
        <div class="logo">Регистрация</div>
        <form method="post" action="/user/adduser">
            <label>Имя пользователя</label><br>
            <input type="text" name="username"><br>
            <label>Email пользователя</label><br>
            <input type="email" name="email"><br>
            <label>Пароль пользователя</label><br>
            <input type="password" name="password" min="8"><br>
            <input type="submit" name="reg" value="Регистрация" class="btn btn-success" onclick="return getValidate(this.form)">
        </form>
        <a href="/user/avtorizuser">Авторизация</a>