<?php
/**
 * Created by PhpStorm.
 * User: nikolai
 * Date: 09.02.16
 * Time: 10:38
 */
?>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title>header</title>
    <link type="text/css" rel="stylesheet" href="../styles/bootstrap.min.css">
    <link type="text/css" rel="stylesheet" href="../styles/styles.css">
    <script type="text/javascript" rel="javascript" src="script/js.js"></script>
    <script type="text/javascript" rel="javascript" src="script/jquery-2.2.1.min.js"></script>
    <script type="text/javascript" rel="javascript" src="script/bootstrap.min.js"></script>
</head>
<body>
    <div class="container">
    <?php include_once 'Views/'.$view.'.php' ?>
    </div>
</body>
</html>