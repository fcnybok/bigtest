<?php
/**
 * Created by PhpStorm.
 * User: nikolai
 * Date: 09.02.16
 * Time: 10:53
 */
?>
<div class="head">Файлы пользователя <span><?php echo $_SESSION['user']['name'] ?></span>
    <div><a href="/user/addfileform"><button class="btn btn-success">Добавить файл</button></a> || <a href="/user/logout"><button class="btn btn-success">Выход</button></a></div>
</div>
<div class="content">
    <table class="table .table-striped table-bordered table-hover">
        <tr>
            <th>Номер</th>
            <th>Название</th>
            <th>Ссылка</th>
            <th>Размер</th>
            <th>Время загрузки</th>
        </tr>
<?php
if(is_array($data)) {
    $i = 0;
    foreach ($data as $file_user_value) {
        ++$i;
        echo "<tr>
                <td>$i</td>
                <td>{$file_user_value['file_name']}</td>
                <td><a href={$file_user_value['file_link']}>{$file_user_value['file_link']}</a></td>
                <td>{$file_user_value['file_size']}</td>
                <td>{$file_user_value['datainsert']}</td>
        </tr>";
    }
}
?>
    </table>
<!---->
</div>