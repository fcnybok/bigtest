-- phpMyAdmin SQL Dump
-- version 4.4.15.5
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Июн 17 2016 г., 07:39
-- Версия сервера: 5.6.29
-- Версия PHP: 7.0.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `test`
--

-- --------------------------------------------------------

--
-- Структура таблицы `file_user`
--

CREATE TABLE IF NOT EXISTS `file_user` (
  `id_file` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `file_name` varchar(100) NOT NULL,
  `file_link` varchar(200) NOT NULL,
  `file_size` int(11) NOT NULL,
  `datainsert` datetime NOT NULL,
  `status` tinyint(4) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `file_user`
--

INSERT INTO `file_user` (`id_file`, `id_user`, `file_name`, `file_link`, `file_size`, `datainsert`, `status`) VALUES
(10, 2, 'anketa.xls', 'fileusers/fcnybok/anketa.xls', 368128, '2016-06-16 22:41:06', 0),
(11, 2, 'css.pdf', 'fileusers/fcnybok/css.pdf', 73134, '2016-06-16 22:41:42', 0),
(12, 2, 'html5-cheat-sheet (copy 2).pdf', 'fileusers/fcnybok/html5-cheat-sheet (copy 2).pdf', 76815, '2016-06-17 00:32:58', 0),
(13, 13, 'angular.min.js', 'fileusers/dagger/angular.min.js', 147735, '2016-06-17 00:52:45', 0),
(14, 13, 'angular.min.js', 'fileusers/dagger/angular.min.js', 147735, '2016-06-17 00:52:46', 0);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `userid` int(11) NOT NULL,
  `username` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`userid`, `username`, `email`, `password`) VALUES
(2, 'fcnybok', 'fcnybok@tut.by', '25d55ad283aa400af464c76d713c07ad'),
(4, 'user001', 'fear_killer@mail.ru', '25d55ad283aa400af464c76d713c07ad'),
(8, 'user002', 'test@tut.by', '25d55ad283aa400af464c76d713c07ad'),
(9, 'pgg', 'pgg@tut.by', '25d55ad283aa400af464c76d713c07ad'),
(13, 'dagger', 'dagger@tut.by', '25d55ad283aa400af464c76d713c07ad');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `file_user`
--
ALTER TABLE `file_user`
  ADD PRIMARY KEY (`id_file`),
  ADD KEY `FK_file_user_user` (`id_user`),
  ADD FULLTEXT KEY `file_name` (`file_name`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userid`),
  ADD UNIQUE KEY `username_email` (`username`,`email`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `file_user`
--
ALTER TABLE `file_user`
  MODIFY `id_file` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `userid` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `file_user`
--
ALTER TABLE `file_user`
  ADD CONSTRAINT `FK_file_user_user` FOREIGN KEY (`id_user`) REFERENCES `users` (`userid`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
