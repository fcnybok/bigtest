<?php
/**
 * Created by PhpStorm.
 * User: �������
 * Date: 14.06.2016
 * Time: 18:44
 */

include_once 'config.php';
function __autoload($className){
    $className=trim($className);
    if(preg_match('/Controller_.[A-Za-z0-9_]+/', $className)){
        require_once 'class/controller/'.$className.'.php';
        return;
    }
    if(preg_match('/Model_.[A-Za-z0-9_]+/', $className)){
        require_once 'class/Models/'.$className.'.php';
        return;
    }else{
        require_once 'class/base/'.$className.'.php';
        return;
    }

}
Route::router();


