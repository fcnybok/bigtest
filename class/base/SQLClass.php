<?php

/**
 * Created by PhpStorm.
 * User: �������
 * Date: 08.02.2016
 * Time: 21:51
 */
//include_once './config.php';
//Класс для работы с базой данных
class SQLClass{
    protected static $sql;

    private function __construct(){
        $dsn = 'mysql:dbname='.DB_NAME.';host='.DB_HOST;
        $user = DB_USER;
        $password = DB_PASSWORD;
        try {
            $this->connect = new PDO($dsn, $user, $password);
        } catch (PDOException $e) {
            echo 'Connection failed: ' . $e->getMessage();
        }
    }
    public static function addUsers($name, $email, $password){
        $sql=self::getSQLobj();
        if(isset($sql)){
            try {
                $ins=$sql->prepare("INSERT INTO users (username, email, password) VALUES (:username, :email, :password)", array(PDO::ATTR_CURSOR=>PDO::CURSOR_SCROLL));
                if(!$ins->execute(array(':username'=>$name, ':email'=>$email, ':password'=>$password))){
                    die('Не удалос добавить пользователя');
                }else{
                    return true;
                }
            }catch (PDOException $e){
                $e->getMessage();
            }
        }
    }
    public static function selectUser($email, $password){
        $sql=self::getSQLobj();
        if(isset($sql)){
            try {
                $sel=$sql->prepare("SELECT userid ,username, email, password FROM users WHERE email=:email AND password=:password", array(PDO::ATTR_CURSOR=>PDO::CURSOR_SCROLL));
                $sel->execute(array(':email'=>$email, ':password'=>$password));
                $result=$sel->fetch(PDO::FETCH_ASSOC);
                if($result){
                    $_SESSION['user']['name']=$result['username'];
                    $_SESSION['user']['id']=$result['userid'];
                    return true;
                }else return false;
            }catch (PDOException $e){
                $e->getMessage();
            }
        }
    }
    public static function selectUserFile(){
        $objSQL=self::getSQLobj();
        if(isset($objSQL)){
            try {
                $sel=$objSQL->prepare("SELECT file_name , file_link, file_size, datainsert FROM file_user WHERE id_user=:id ORDER BY datainsert",
                                       array(PDO::ATTR_CURSOR=>PDO::CURSOR_SCROLL));
                $sel->execute(array('id'=>$_SESSION['user']['id']));
                $result=$sel->fetchAll(PDO::FETCH_ASSOC);
                if($result){
                    return $result;
                }else return false;
            }catch (PDOException $e){
                $e->getMessage();
            }
        }
    }
    public static function addFileDB($param){
        $objSQL=self::getSQLobj();

        if(isset($objSQL)){
            try {
                $ins=$objSQL->prepare("INSERT INTO file_user (id_user, file_name, file_link, file_size, datainsert) VALUES (:id_user, :file_name,
                                                              :file_link, :file_size, :file_date)", array(PDO::ATTR_CURSOR=>PDO::CURSOR_SCROLL));
                if($ins->execute(array(':id_user'=>(int)$_SESSION['user']['id'], ':file_name'=>$param[0], ':file_link'=>$param[1],
                                    ':file_size'=>(int)$param[2], ':file_date'=>date("YmdHis")))){
                    echo "Файл успешно добавлен в БД";
                    return true;
                }else return false;
            }catch (PDOException $e){
                $e->getMessage();
            }
        }
    }

    public static function connect(){
        if(self::$sql===null){
            self::$sql=new self;
        }
        return self::$sql;
    }

    private static function getSQLobj(){
        return self::connect()->connect;
    }

    private function __clone(){

    }
    private function __wakeup(){

    }
}