<?php

/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 15.06.2016
 * Time: 22:43
 */
class Controller_user extends Controller{
    function __construct(){
        parent::__construct();
        $this->model=new Model_main();
    }
    public function avtorizuser(){
        $this->View->getGenerate('avtorizac');
    }
    public function avtoriz(){
        $this->model->avtUser();
    }
    public function addfile(){
        $this->model->addFileUser();
        $this->View->getGenerate('addform');
    }
    public function addfileform(){
        $this->View->getGenerate('addform');
    }
    public function registration(){
        $this->View->getGenerate('registration');
    }
    public function adduser(){
        $this->model->registrationUser();
    }
    public function logout(){
        session_destroy();
        $this->View->getGenerate('avtorizac');
    }
}