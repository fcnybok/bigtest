<?php

/**
 * Created by PhpStorm.
 * User: Николай
 * Date: 14.06.2016
 * Time: 20:06
 */
class Model_main extends Model
{
//    public function getData()
//    {
//        return array(1, 2, 3);
//    }

    public function regUser()
    {
        if ($_POST['reg']) {
            if (isset($_POST['username']) || isset($_POST['email']) || isset($_POST['username'])) {
                $user_name = parent::stringPost($_POST['username']);
                $user_email = parent::stringPost($_POST['email']);
                $user_password = parent::stringPost($_POST['password']);
                if (strlen($user_password) < 8) {
                    echo "Пароль меньше восьми символов";
                    exit;
                } else {
                    $user_password = md5($user_password);
                }
                SQLClass::addUsers($user_name, $user_email, $user_password);
            }
        }
    }

    public function avtUser()
    {
        if ($_POST['avt']) {
            if (isset($_POST['email']) || isset($_POST['username'])) {
                $user_email = parent::stringPost($_POST['email']);
                $user_password = md5(parent::stringPost($_POST['password']));
                if (SQLClass::selectUser($user_email, $user_password)) {
                    header('Location: /');
                } else {
                    return false;
                }
            }
        }
    }

    public function UserFile()
    {
        return SQLClass::selectUserFile();
    }

    public function addFileUser()
    {
        $params = $this->addFileDir();
        if ($params) {
            SQLClass::addFileDB($params);
        }
    }

    private function addFileDir()
    {
        if (isset($_FILES['filename']) || isset($_POST['load'])) {
            $file = $_FILES['filename'];
            $username = $_SESSION['user']['name'];
            $userdir = $_SERVER['DOCUMENT_ROOT'] . '/fileusers/' . $username;
            if (!is_dir($userdir)) {

                if (!mkdir($userdir, 0777)) {
                    die('Не удалось создать');
                }
            }
//        имя файла
            $basename = basename($file['name']);
            $filesize = $file['size'];
            if ($filesize == 0) {
                return;
            }
            if (move_uploaded_file($file['tmp_name'], $userdir . '/' . $basename)) {
                echo 'Файл загружен';
            }
            $filelink = 'fileusers/' . $username . '/' . $basename;

            return array($basename, $filelink, $filesize);
        } else {
            return false;
        }
    }

    public function registrationUser()
    {
        if ($_POST['reg']) {
            if (isset($_POST['username']) || isset($_POST['email']) || isset($_POST['username'])) {
                $user_name = parent::stringPost($_POST['username']);
                $user_email = parent::stringPost($_POST['email']);
                $user_password = parent::stringPost($_POST['password']);
                if (strlen($user_password) < 8) {
                    echo "Пароль меньше восьми символов";
                    exit;
                } else {
                    $user_password = md5($user_password);
                }
                if (SQLClass::addUsers($user_name, $user_email, $user_password)) {
                    header('Location: /');
                }
            }
        }
    }
}